import random

# Constants for board size and initial cup counts
BOARD_SIZE = 3
PLAYER_CUPS = [1, 1, 2, 2, 3, 3]
COMPUTER_CUPS = [1, 1, 2, 2, 3, 3]


def create_board():
    """Create the initial game board."""
    return [['-' for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]


def is_tie(board):
    """Check for a tie in the game."""
    for row in board:
        if '-' in row:
            return False
    return not PLAYER_CUPS and not COMPUTER_CUPS


def is_winner(board, player):
    """Check if a player has won the game."""
    symbols = ["X1", "X2", "X3"] if player == 'X' else ["O1", "O2", "O3"]

    # Check rows, columns, and diagonals for a win
    for i in range(BOARD_SIZE):
        if all(board[i][j][0] == player for j in range(BOARD_SIZE) if board[i][j] != '-'):
            return True
        if all(board[j][i][0] == player for j in range(BOARD_SIZE) if board[j][i] != '-'):
            return True

    if all(board[i][i][0] == player for i in range(BOARD_SIZE) if board[i][i] != '-'):
        return True
    if all(board[i][BOARD_SIZE-i-1][0] == player for i in range(BOARD_SIZE) if board[i][BOARD_SIZE-i-1] != '-'):
        return True

    return False


def validate_move(board, move, cup_num, player):
    """Validate the player's move."""
    row, col = move
    if board[row][col] == '-' or (board[row][col][0] != player and int(board[row][col][1]) < cup_num):
        return True
    return False


def update_board(board, move, cup_num, player):
    """Update the board with the player's move."""
    row, col = move
    board[row][col] = player + str(cup_num)
    if player == 'X':
        PLAYER_CUPS.remove(cup_num)
    else:
        COMPUTER_CUPS.remove(cup_num)


def get_computer_move(board):
    """
    Get the computer's move.
    The computer selects a valid spot based on the game's rules and available cups.
    """

    available_moves = []
    biggest_comp_cup = max(COMPUTER_CUPS) if COMPUTER_CUPS else 0

    # Loop through the board to find valid moves
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            cell = board[row][col]
            if cell == '-':
                available_moves.append((row, col, min(COMPUTER_CUPS)))
            elif cell[0] != 'O' and int(cell[1]) < biggest_comp_cup:
                # Find a cup size that can be placed on top of the current cup
                valid_cups = [
                    cup for cup in COMPUTER_CUPS if cup > int(cell[1])]
                if valid_cups:
                    available_moves.append((row, col, min(valid_cups)))

    # If no valid moves, return a message indicating no possible move
    if not available_moves:
        return None, "No valid moves available"

    # Randomly choose a move from available moves
    chosen_move = random.choice(available_moves)
    row, col, cup_num = chosen_move

    # Update COMPUTER_CUPS to remove the used cup
    COMPUTER_CUPS.remove(cup_num)

    return (row, col), "O" + str(cup_num)


def get_human_move(board: list, move: tuple) -> bool:
    """
    Validate the human player's move.

    Args:
    board (list): The current game board.
    move (tuple): The row and column indices of the chosen cell.

    Returns:
    bool: True if the move is valid, False otherwise.
    """
    row, col = move

    # Validate the move
    if 0 <= row < BOARD_SIZE and 0 <= col < BOARD_SIZE:
        if board[row][col] == '-' or (board[row][col][0] != 'O' and board[row][col][0] != 'X'):
            return True

    return False
