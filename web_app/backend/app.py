# app.py

from flask import Flask, jsonify, request
from flask_cors import CORS
from stacking import create_board, get_human_move, get_computer_move, is_winner, is_tie

app = Flask(__name__)
CORS(app, resources={r'/*': {'origins': '*'}})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)

# Initialize the game board
board = create_board()


@app.route('/', methods=['GET'])
def home():
    return "Flask Backend Running"


@app.route('/move', methods=['POST'])
def make_move():
    global board
    data = request.json
    # Extract move and cup number from the request
    move = data['move']
    cup_num = data['cupNum']

    # Update board with human's move
    board[move[0]][move[1]] = 'X' + str(cup_num)
    if is_winner(board, 'X'):
        return jsonify({'board': board, 'status': 'Human wins!'})

    # Get computer's move
    computer_move, move_str = get_computer_move(board)
    row, col = computer_move
    cup_num = int(move_str[1])
    board[row][col] = move_str

    return jsonify({'row': row + 1, 'col': col + 1, 'cupNum': cup_num})


if __name__ == '__main__':
    app.run(debug=True)
