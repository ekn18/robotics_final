import { useCallback, useMemo, useState } from "react";
import "./App.css";

const port = "5001";
const backend_url = `http://localhost:${port}`;

// Adapted from React Tutorial: Tic-Tac-Toe(https://react.dev/learn/tutorial-tic-tac-toe)
function Square({ value, onSquareClick }) {
  return (
    <button className="square" onClick={onSquareClick}>
      {value}
    </button>
  );
}

function Board({ xIsNext, squares, onPlay, selectedSideButton }) {
  function convertIndexToCoordinates(index) {
    const row = Math.floor(index / 3);
    const col = index % 3;
    return [row, col];
  }

  function handleClick(i) {
    const currentPlayer = xIsNext ? "X" : "O";
    const currentButton = selectedSideButton[currentPlayer];

    if (calculateWinner(squares) || !currentButton) {
      return;
    }

    // Allow placing on top if the square is empty or the current cup is larger than the one on the square
    if (
      !squares[i] ||
      parseInt(currentButton.value) > parseInt(squares[i][1])
    ) {
      const nextSquares = squares.slice();
      nextSquares[i] = currentPlayer + currentButton.value;
      onPlay(nextSquares, convertIndexToCoordinates(i), i);
    }
  }

  return (
    <>
      <div className="board-row">
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
      </div>
      <div className="board-row">
        <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
        <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
        <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
      </div>
      <div className="board-row">
        <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
        <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
        <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
      </div>
    </>
  );
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];

    // Check if all squares in the line are non-null
    if (squares[a] && squares[b] && squares[c]) {
      // Get the topmost cup on each square
      const topmostA = squares[a].slice(-2);
      const topmostB = squares[b].slice(-2);
      const topmostC = squares[c].slice(-2);

      // Check if the topmost cups are from the same player and if they are lined up
      if (topmostA[0] === topmostB[0] && topmostA[0] === topmostC[0]) {
        return topmostA[0] === "X" ? "Human" : "Robot";
      }
    }
  }

  return null;
}

function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const xIsNext = useMemo(() => currentMove % 2 === 0, [currentMove]);
  const currentSquares = useMemo(
    () => history[currentMove],
    [history, currentMove]
  );
  const [selectedSideButton, setSelectedSideButton] = useState({
    X: null,
    O: null,
  });
  const [usedButtons, setUsedButtons] = useState([]);
  const [robotMove, setRobotMove] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  function isBoardFull(squares) {
    return squares.every((square) => square != null);
  }

  function isMoveLeft(selectedSideButton) {
    // Ensure that selectedSideButton for both X and O are defined before checking their values
    const xButtons = selectedSideButton.X
      ? Object.values(selectedSideButton.X)
      : [];
    const oButtons = selectedSideButton.O
      ? Object.values(selectedSideButton.O)
      : [];
    return xButtons.concat(oButtons).some((button) => button != null);
  }

  const winner = useMemo(
    () => calculateWinner(currentSquares),
    [currentSquares]
  );
  const status = useMemo(() => {
    if (isLoading) {
      return "Loading...";
    }

    if (winner) {
      return "Winner: " + winner;
    } else if (isBoardFull(currentSquares) && !isMoveLeft(selectedSideButton)) {
      return "Game Over: Tie"; // Set status to draw if all buttons are used and it's a full board
    }
    // else if (!isMoveLeft(selectedSideButton)) {
    //   return 'Game Over: Draw'; // Set status to draw if all buttons are used and it's a full board
    // }
    else {
      return "Next player: " + (xIsNext ? "Human" : "Robot");
    }
  }, [currentSquares, selectedSideButton, xIsNext, isLoading, winner]);

  function handleSideButtonClick(player, value, id) {
    // When a side button is clicked and the button hasn't been used, select it
    const isUsed = usedButtons.some((button) => button.id === id);
    if (!isUsed) {
      setSelectedSideButton((prev) => ({ ...prev, [player]: { value, id } }));
    }
  }

  // function handlePlay(nextSquares) {
  //   const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
  //   setHistory(nextHistory);
  //   setCurrentMove(nextHistory.length - 1);

  //    // Add the selected button to the usedButtons array to prevent it from being used again
  //    setUsedButtons(prev => [...prev, selectedSideButton[xIsNext ? 'X' : 'O']]);
  //    // Clear the selected side button for the current player after placing a move
  //    setSelectedSideButton(prev => ({ ...prev, [xIsNext ? 'X' : 'O']: null }));

  //    setRobotMove(null);
  // }
  function handlePlay(nextSquares, move, cupNum) {
    // Update the game history with the new move
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);

    // Add the selected button to the usedButtons array to prevent it from being used again
    setUsedButtons((prev) => [
      ...prev,
      selectedSideButton[xIsNext ? "X" : "O"],
    ]);

    // Clear the selected side button for the current player after placing a move
    setSelectedSideButton((prev) => ({ ...prev, [xIsNext ? "X" : "O"]: null }));

    // Send the human player's move to the backend
    if (xIsNext) {
      setIsLoading(true);
      fetch(`${backend_url}/move`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ move, cupNum }),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          // Update the board with the response from the backend
          setHistory((prev) => [...prev.slice(0, currentMove + 1), data.board]);
          setCurrentMove((prev) => prev + 1);

          // Update robotMove with the robot's choice
          setRobotMove(
            `Robot chose cup ${data.cupNum} at position (${data.row}, ${data.col})`
          );
          setIsLoading(false);
        })
        .catch((error) => {
          console.error("Error:", error);
          setIsLoading(false);
        });
    }
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  const moves = useMemo(
    () =>
      history.map((squares, move) => (
        <li key={move}>
          <button onClick={() => jumpTo(move)}>
            {move > 0 ? "Go to move #" + move : "Go to game start"}
          </button>
        </li>
      )),
    [history]
  );

  const createSideButtons = (player: string) => {
    return [1, 1, 2, 2, 3, 3].map((value, index) => {
      const id = `${player}-${value}-${index}`;
      const isUsed = usedButtons.some((button) => button && button.id === id);
      const isSelected =
        selectedSideButton[player] && selectedSideButton[player].id === id;
      return isUsed ? null : (
        <button
          key={id}
          className={isSelected ? "selected" : ""}
          onClick={() => handleSideButtonClick(player, value, id)}
          disabled={
            isUsed || isSelected || (player === "X" ? !xIsNext : xIsNext)
          }
        >
          {value}
        </button>
      );
    });
  };

  const xSideButtons = createSideButtons("X");
  const oSideButtons = createSideButtons("O");

  return (
    <div className="game">
      {robotMove && <div className="robot-move">{robotMove}</div>}
      <div className="game-board">
        <div className="status">{status}</div>
        <Board
          xIsNext={xIsNext}
          squares={currentSquares}
          onPlay={handlePlay}
          selectedSideButton={selectedSideButton}
        />
      </div>
      <div className="side-buttons-container">
        <div className="side-buttons x-side-buttons">{xSideButtons}</div>
        <div className="side-buttons o-side-buttons">{oSideButtons}</div>
      </div>
      <div className="game-info">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}

function App() {
  return <Game />;
}

export default App;
