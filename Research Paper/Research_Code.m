%% Inverse Kinematics: Go to Spot 7 starting with x,y,z coordinates

close all;
clear all;
clc;

% Load UR5 robot in simulation
ur5 = loadrobot("universalUR5")

% Defined starting position by joint angles for tic-tac-toe game
starting_position = [0 -90 90 -90 -90 -90]*pi/180;

% Defining inverse kinematics for UR5
ik = inverseKinematics("RigidBodyTree",ur5);

% Defining joint names given by documentation and showdetails(ur5)
joints = {'shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint'};

% Prioritizing x,y,z coordinates over orientation
weights = [1 1 1 .1 .1 .1];

% Starting the timer to determine computational time
tic;

% Create a struct for the starting position to pass it through IK function
starting_position = struct('JointName', joints, 'JointPosition', num2cell(starting_position));

% Transformation matrix from starting position to desired end position
rotation_quaternion = axang2quat([0, 1, 0, pi/2]); % axis-angle to quaternion conversion, rotating 90 degrees about y-axis
desired_pose = trvec2tform([0.85, -.15, .18]) * quat2tform(rotation_quaternion); % combining translation and rotation matrix

% Using inverse kinematics to provide the desired joint angles
[inv_values,inv_info] = ik("ee_link",desired_pose,weights,starting_position)

% Ending timer for computational time
computational_time = toc;

% Indexing information from inverse kinematics computation
iteration_count = inv_info.Iterations;
position_error = inv_info.PoseErrorNorm;

% Printing Computational Values
fprintf('Computational Time: %.2f\n', computational_time);
fprintf('Number of Iterations: %.2f\n', iteration_count);
fprintf('Error from Desired Postion:  %.6e\n', position_error);

% Plotting Starting and Final Positions
figure('Position', [100, 100, 1200, 600]); % [left, bottom, width, height]

% Subplot 1: Starting Position
subplot(1, 2, 1); 
show(ur5, starting_position);
title('Forward Kinematics - Starting Position');

% Subplot 2: Final Position
subplot(1, 2, 2); 
show(ur5, inv_values);
title('Forward Kinematics - Final Position');
%% Forward Kinematics: Go to Spot 7 starting with joint goals

clear all
close all
clc

% Load UR5 robot in simulation
ur5 = loadrobot("universalUR5");

% Defining joint names given by documentation and showdetails(ur5)
joints = {'shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint'};

% Define joint angles for the starting position
starting_position = [0 -90 90 -90 -90 -90]*pi/180;

% Create a struct for the starting position to pass it through FK function
starting_position = struct('JointName', joints, 'JointPosition', num2cell(starting_position));

% Define joint angles for the ending position
final_position = [-0.301469485747035 -0.510442413911348 0.598506247686651 -1.65886015950333 -1.57079632615563 -0.301469482528778];

% Create a struct for the ending position to pass it through FK function
final_position = struct('JointName', joints, 'JointPosition', num2cell(final_position));

% Starting the timer for forward kinematics
tic;

% Calculate the end effector starting pose using forward kinematics
starting_pose = getTransform(ur5,starting_position,"ee_link");

% Calculate the end effector ending pose using forward kinematics
ending_pose = getTransform(ur5, final_position, "ee_link");

% Ending the timer for forward kinematics
forward_computational_time = toc;

% Extract position and orientation from the homogeneous transformation matrices
position_start = starting_pose(1:3, 4);
orientation_start = rotm2eul(starting_pose(1:3, 1:3), 'XYZ'); % Convert rotation matrix to Euler angles

position_final = ending_pose(1:3, 4);
orientation_final = rotm2eul(ending_pose(1:3, 1:3), 'XYZ'); % Convert rotation matrix to Euler angles

% Display the starting and final end effector positions and orientations
fprintf('Computational Time for Forward Kinematics: %.6f seconds\n', forward_computational_time);

fprintf('Starting End Effector Position (XYZ): %.4f, %.4f, %.4f [m]\n', position_start);
fprintf('Starting End Effector Orientation (RPY): %.4f, %.4f, %.4f [rad]\n', orientation_start);

fprintf('Final End Effector Position (XYZ): %.4f, %.4f, %.4f [m]\n', position_final);
fprintf('Final End Effector Orientation (RPY): %.4f, %.4f, %.4f [rad]\n', orientation_final);

% Plotting Starting and Final Positions
figure('Position', [100, 100, 1200, 600]); % [left, bottom, width, height]

% Subplot 1: Starting Position
subplot(1, 2, 1); 
show(ur5, starting_position);
title('Forward Kinematics - Starting Position');

% Subplot 2: Final Position
subplot(1, 2, 2); 
show(ur5, final_position);
title('Forward Kinematics - Final Position');