"""
ORIGNAL Tic Tac Toe - Human vs Computer

Code from: https://codepal.ai/code-generator/query/NMSBP7z8/tic-tac-toe-human-vs-computer
This program allows a human player to play Tic Tac Toe against the computer.
The game is played on a 3x3 grid, and the players take turns marking a cell with their respective symbols (X or O).
The first player to get 3 of their symbols in a row (horizontally, vertically, or diagonally) wins the game.

"""

import random
import logging

# Setting up logging to monitor performance and errors
logging.basicConfig(level=logging.INFO)

# Constants for player symbols
PLAYER_SYMBOL = "X"
COMPUTER_SYMBOL = "O"

# Constants for board size
BOARD_SIZE = 3

def create_board() -> list:
    """
    Create the initial game board.

    Returns:
    list: The initial game board.

    Examples:
    >>> create_board()
    [['-', '-', '-'], ['-', '-', '-'], ['-', '-', '-']]
    """
    return [['-' for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]

def print_board(board: list) -> None:
    """
    Print the current game board.

    Args:
    board (list): The current game board.

    Returns:
    None

    Examples:
    >>> board = [['X', 'O', '-'], ['-', 'X', '-'], ['-', '-', 'O']]
    >>> print_board(board)
    X O -
    - X -
    - - O
    """
    for row in board:
        print(" ".join(row))

def is_board_full(board: list) -> bool:
    """
    Check if the game board is full.

    Args:
    board (list): The current game board.

    Returns:
    bool: True if the board is full, False otherwise.

    Examples:
    >>> board = [['X', 'O', '-'], ['-', 'X', '-'], ['-', '-', 'O']]
    >>> is_board_full(board)
    False

    >>> board = [['X', 'O', 'X'], ['X', 'O', 'O'], ['O', 'X', 'X']]
    >>> is_board_full(board)
    True
    """
    for row in board:
        if '-' in row:
            return False
    return True

def is_winner(board: list, symbol: str) -> bool:
    """
    Check if a player has won the game.

    Args:
    board (list): The current game board.
    symbol (str): The player's symbol.

    Returns:
    bool: True if the player has won, False otherwise.

    Examples:
    >>> board = [['X', 'O', '-'], ['-', 'X', '-'], ['-', '-', 'O']]
    >>> is_winner(board, 'X')
    True

    >>> board = [['X', 'O', '-'], ['-', 'X', '-'], ['-', '-', 'O']]
    >>> is_winner(board, 'O')
    False
    """
    # Check rows
    for row in board:
        if row.count(symbol) == BOARD_SIZE:
            return True

    # Check columns
    for col in range(BOARD_SIZE):
        if [board[row][col] for row in range(BOARD_SIZE)].count(symbol) == BOARD_SIZE:
            return True

    # Check diagonals
    if [board[i][i] for i in range(BOARD_SIZE)].count(symbol) == BOARD_SIZE:
        return True
    if [board[i][BOARD_SIZE - i - 1] for i in range(BOARD_SIZE)].count(symbol) == BOARD_SIZE:
        return True

    return False

def get_human_move(board: list) -> tuple:
    """
    Get the human player's move.

    Args:
    board (list): The current game board.

    Returns:
    tuple: The row and column indices of the chosen cell.

    Examples:
    >>> board = [['X', 'O', '-'], ['-', 'X', '-'], ['-', '-', 'O']]
    >>> get_human_move(board)
    Enter your move (row, col): 0, 2
    (0, 2)
    """
    while True:
        try:
            move = input("Enter your move (row, col): ")
            row, col = map(int, move.split(","))
            if 0 <= row < BOARD_SIZE and 0 <= col < BOARD_SIZE and board[row][col] == '-':
                return row, col
            else:
                print("Invalid move. Please try again.")
        except ValueError:
            print("Invalid input. Please enter row and column indices separated by a comma.")

def get_computer_move(board: list) -> tuple:
    """
    Get the computer's move.

    Args:
    board (list): The current game board.

    Returns:
    tuple: The row and column indices of the chosen cell.

    Examples:
    >>> board = [['X', 'O', '-'], ['-', 'X', '-'], ['-', '-', 'O']]
    >>> get_computer_move(board)
    (0, 2)
    """
    available_moves = []
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if board[row][col] == '-':
                available_moves.append((row, col))
    return random.choice(available_moves)

def play_game() -> None:
    """
    Play a game of Tic Tac Toe.

    Returns:
    None
    """
    board = create_board()
    print("Welcome to Tic Tac Toe!")
    print("You are X and the computer is O.")
    print("Enter your move as row and column indices separated by a comma.")
    print("The board is a 3x3 grid with the top-left cell as (0, 0).")
    print("Let's begin!\n")
    print_board(board)

    while True:
        # Human player's turn
        human_move = get_human_move(board)
        board[human_move[0]][human_move[1]] = PLAYER_SYMBOL
        print_board(board)
        if is_winner(board, PLAYER_SYMBOL):
            print("Congratulations! You win!")
            break
        if is_board_full(board):
            print("It's a tie!")
            break

        # Computer's turn
        computer_move = get_computer_move(board)
        board[computer_move[0]][computer_move[1]] = COMPUTER_SYMBOL
        print("The computer has made its move.")
        print_board(board)
        if is_winner(board, COMPUTER_SYMBOL):
            print("Sorry, you lose!")
            break
        if is_board_full(board):
            print("It's a tie!")
            break

if __name__ == "__main__":
    play_game()