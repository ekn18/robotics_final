
  
"""

Stacking Tic Tac Toe - Human vs Computer

Adapted from the Original Tic Tac Toe code: https://codepal.ai/code-generator/query/NMSBP7z8/tic-tac-toe-human-vs-computer
 

Implementation Notes:

Computer now chooses from spots where the human player has a cup placed already. 
If it doesn't have a cup large enough to fit over those placed, it chooses a blank spot and its cup number randomly. 
Computer doesn't stack on its own cups currently, but the human player is allowed. 


Stacking Tic Tac Toe rules:
This program allows a human player to play stacking Tic Tac Toe against the computer in terminal.
The game is played on a 3x3 grid, and the players take turns marking a cell with their respective symbols that repesent the cups (X1,X2,X3,X4,X5,X6 or O1,O2,O3,O4,O5,O6).
Players can stack their cups on opposing team cups, as long as the cup number (size) is greater than the current one placed.
The first player to get 3 of their symbols in a row (horizontally, vertically, or diagonally) wins the game.
 
"""
 
import random
import logging
 
# Setting up logging to monitor performance and errors
logging.basicConfig(level=logging.INFO)
 
#player symbols and cup numbers
PLAYER_SYMBOLS = ["X1", "X2", "X3", "X4", "X5", "X6"]
PLAYER_CUPS = [1,2,3,4,5,6]
COMPUTER_SYMBOLS = ["O1", "O2", "O3", "O4", "O5", "O6"]
COMPUTER_CUPS = [1,2,3,4,5,6]
 
# Constants for board size
BOARD_SIZE = 3
 
def create_board() -> list:
    """
    Create the initial game board.
 
    Returns:
    list: The initial game board.
 
    Examples:
    >>> create_board()
    [['-', '-', '-'], ['-', '-', '-'], ['-', '-', '-']]
    """
    return [['-' for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]
 
def print_board(board: list) -> None:
    """
    Print the current game board.
 
    Args:
    board (list): The current game board.
 
    Returns:
    None

    """
    for row in board:
        print(" ".join(row))
 
def is_tie(board: list) -> bool:
    """
    Checks if there is a tie by checking if the game board is full or 
    if either players have ran out of cups to place.
 
    Args:
    board (list): The current game board.
 
    Returns:
    bool: True if the board is full or if either player ran out of cups, False otherwise.

    """

    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if board[row][col] == '-':
                return False
    if PLAYER_CUPS: # not empty
        return False
    if COMPUTER_CUPS:
        return False

    return True

def is_winner(board: list, symbols: list) -> bool:
    """
    Check if a player has won the game by having three of 
    their pieces in a row (horizontally, vertically, or diagonally).
 
    Args:
    board (list): The current game board.
    symbol (str): The player's symbol.
 
    Returns:
    bool: True if the player has won, False otherwise.

    """
    count = 0
    for row in board:
        count = 0
        for i in range(len(symbols)):
            count += row.count(symbols[i])
        if count == BOARD_SIZE:
            return True
 
    # Check columns
    for col in range(BOARD_SIZE):
        count = 0
        for i in range(len(symbols)):
            count +=[board[row][col] for row in range(BOARD_SIZE)].count(symbols[i]) 
        if count == BOARD_SIZE:
            return True
 
    # Check diagonals
    count = 0
    for i in range(len(symbols)):
        count += [board[j][j] for j in range(BOARD_SIZE)].count(symbols[i])
    if count == BOARD_SIZE:
        return True

    count = 0
    for i in range(len(symbols)):
        count += [board[j][BOARD_SIZE - j - 1] for j in range(BOARD_SIZE)].count(symbols[i])
    if count == BOARD_SIZE:
            return True
 
    return False
 
def get_human_move(board: list) -> tuple: 
    """
    Get the human player's move by prompting them to enter the row,col (indexed 0) of their board choice.
    This function checks the input from the player to make sure it is a valid choice. 
    If the choice is invalid, it'll prompt the user to choose again.
 
    Args:
    board (list): The current game board.
 
    Returns:
    tuple: The row and column indices of the chosen cell.

    """
    while True:
        try:
            move = input("Enter your move (row, col): ")
            row, col = map(int, move.split(","))
            if 0 <= row < BOARD_SIZE and 0 <= col < BOARD_SIZE and board[row][col] != 'O6' and board[row][col] != 'X6': #need to change if human not allowed to stack on its own cups
                return row, col
            else:
                print("Invalid move. Please try again.")
        except ValueError:
            print("Invalid input. Please enter row and column indices separated by a comma.")


def get_human_cup_num(board: list, human_move:tuple) -> int:
    """
    Get the human player's cup number(size) choice by prompting them to enter the cup number from those available.
    This function checks the input from the player to make sure the cup size number is a valid choice. 
    If the choice is invalid, it'll prompt the user to choose again.
    Once the chocie is valid, it will remove that cup size from the available list of player_cups.
 
    Args:
    board (list): The current game board.
    human_move (tuple): (row,col) of their valid move choice
 
    Returns:
    int: Player's valid cup number choice.
 
    """
    while True:
        try:
            cup_num = int(input("What size cup do you want to choose? Available choices are {}: ".format(PLAYER_CUPS)))
            #check if valid cup number based on what is already on board:
            if board[human_move[0]][human_move[1]] != '-':
                curr_cup_num = int(board[human_move[0]][human_move[1]][-1])
                if cup_num <= curr_cup_num:
                    print("Invalid move. Cup size choice must be greater than that on the board. Available options: ", PLAYER_CUPS)
                    continue
                if cup_num not in PLAYER_CUPS:
                    print("Cup number already used. Please choose another. Available cups: ", PLAYER_CUPS)
                    continue
            
            
            PLAYER_CUPS.remove(cup_num)
            return cup_num
        except ValueError:
            print("Invalid cup number. Please try again. Enter a valid cup number. Available cups: ", PLAYER_CUPS)
 
def get_computer_move(board: list) -> tuple:
    """
    Get the computer's move.
    The computer chooses its move by randomly selecting a spot where the human has one of their pieces placed already.
    The computer will then choose an available cup size randomly from those greater than the current placed cup.

    If there are no cups from the human available or the computer doesn't have any cups larger 
    than those placed by the human, it will select an open spot and available cup number randomly.
 
    Args:
    board (list): The current game board.
 
    Returns:
    tuple: (The row and column indices of the chosen cell, the cup number choice)
    
    """

    #list of valid (row,col) moves where the human placed a piece
    available_moves = []

    #list of free spots where no pieces have been placed
    blanks = []

    biggest_comp_cup = max(COMPUTER_CUPS)

    #loop through the board spots and append any available/open moves
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if board[row][col] == '-':
                blanks.append((row, col))
            elif int(board[row][col][-1]) < biggest_comp_cup and board[row][col] not in COMPUTER_SYMBOLS:
                available_moves.append((row, col))

    if available_moves: #if not empty
        choice = random.choice(available_moves)
    else: #when empty 
        choice = random.choice(blanks)

    if board[choice[0]][choice[1]] == '-':
        comp_cup = random.choice(COMPUTER_CUPS)
    
    else:
        curr_cup = int(board[choice[0]][choice[1]][-1])
        valid_cup_plays = [i for i in COMPUTER_CUPS if i > curr_cup]
        comp_cup = random.choice(valid_cup_plays)
   
   
    move_str = "O" + str(comp_cup)
    COMPUTER_CUPS.remove(comp_cup)

    return choice, move_str


 
def play_game() -> None:
    """
    Play a full game of Tic Tac Toe. 
    Continues play until the computer or human win, or the game board is full, or a player runs out of cups.
 
    Returns:
    None
    """
    board = create_board()
    print("Welcome to Tic Tac Toe!")
    print("You are X and the computer is O.")
    print("Enter your move as row and column indices separated by a comma.")
    print("The board is a 3x3 grid with the top-left cell as (0, 0).")
    print("Let's begin!\n")
    print_board(board)
 
    while True:
        # Human player's turn
        human_move = get_human_move(board)
        human_cup= get_human_cup_num(board, human_move)
        human_char = "X" + str(human_cup)
        board[human_move[0]][human_move[1]] = human_char

        #### robot moves cup here for human #####

        print_board(board)
        if is_winner(board, PLAYER_SYMBOLS):
            print("==Congratulations! You win!==")
            break
        if is_tie(board):
            print("==It's a tie!==")
            break
 
        # Computer's turn
        computer_move, computer_move_str = get_computer_move(board)
        comp_cup_num = int(computer_move_str[-1]) #needed for robot movement later
        board[computer_move[0]][computer_move[1]] = computer_move_str

        ### robot moves cup here for itself ######

        print("The computer has made its move. Computer now has {} as availale choices.".format(COMPUTER_CUPS))
        print_board(board)
        if is_winner(board, COMPUTER_SYMBOLS):
            print("==Sorry, you lose!===")
            break
        if is_tie(board):
            print("==It's a tie!==")
            break
 
if __name__ == "__main__":
    play_game()
