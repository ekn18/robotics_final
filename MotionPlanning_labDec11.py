
# general structure of this code follows the following tutorial: 
# http://docs.ros.org/en/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html
# kept variable names mostly the same as the tutorial due to time constraint


#Importing the proper namespace and packages in order to use moveit

from __future__ import print_function
from six.moves import input
from gripperClass import *
import time

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

from math import pi



def __init__():
        
        # initializing `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("midterm", anonymous=True)

        move_group = moveit_commander.MoveGroupCommander("manipulator")

        return  move_group

def go_to_joint_state(move_group):

        # starting point for robot before it makes each move
        # the robot returns to this position after it picks up a new cup and after it places a cup

        tau = 2.0 * pi

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = -tau / 4
        joint_goal[2] = tau/4
        joint_goal[3] = -tau / 4
        joint_goal[4] = -tau/4
        joint_goal[5] = tau/4
        
        # go to move robot to desired postion

        move_group.go(joint_goal, wait=True)

        # ensures no residual movement
        move_group.stop()

        return joint_goal


def pick_up2(move_group):
 
        # Places cup in Spot 1 (first row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .8
        pose_goal.position.y = .3
        pose_goal.position.z = 0.18

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        pick2 = move_group.get_current_pose().pose
        return pick2

def pick_up1(move_group):
 
        # Places cup in Spot 1 (first row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .55
        pose_goal.position.y = .3
        pose_goal.position.z = 0.18

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        pick1 = move_group.get_current_pose().pose
        return pick1

def pick_up3(move_group):
 
        # Places cup in Spot 1 (first row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .7
        pose_goal.position.y = -.3
        pose_goal.position.z = 0.18

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        pick3 = move_group.get_current_pose().pose
        return pick3

def spot_1(move_group):
 
        # Places cup in Spot 1 (first row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .55
        pose_goal.position.y = .15
        pose_goal.position.z = 0.18

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_one = move_group.get_current_pose().pose
        return spot_one

def spot_2(move_group):
 
        # Places cup in Spot 2 (first row, second column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .55
        pose_goal.position.y = 0
        pose_goal.position.z = 0.25

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_two = move_group.get_current_pose().pose
        return spot_two

def spot_3(move_group):
 
        # Places cup in Spot 1 (first row, third column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .55
        pose_goal.position.y = -.15
        pose_goal.position.z = 0.25

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_three = move_group.get_current_pose().pose

        return spot_three

def spot_4(move_group):
 
        # Places cup in Spot 4 (second row, third column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .7
        pose_goal.position.y = -.15
        pose_goal.position.z = 0.25

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_four = move_group.get_current_pose().pose
        return spot_four

def spot_5(move_group):
 
        # Places cup in Spot 5 (second row, second column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .7
        pose_goal.position.y = 0
        pose_goal.position.z = 0.25

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_five = move_group.get_current_pose().pose
        return spot_five

def spot_6(move_group):
 
        # Places cup in Spot 6 (second row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .7
        pose_goal.position.y = .15
        pose_goal.position.z = 0.25

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_six = move_group.get_current_pose().pose
        return spot_six

def spot_8(move_group):
 
        # Places cup in Spot 8 (third row, second column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .85
        pose_goal.position.y = 0
        pose_goal.position.z = 0.18

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_eight = move_group.get_current_pose().pose
        return spot_eight

def spot_7(move_group):
 
        # Places cup in Spot 7 (third row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .85
        pose_goal.position.y = .15

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()
        move_group.clear_pose_targets()
        
        spot_seven = move_group.get_current_pose().pose

        return spot_seven

def spot_7down(move_group):
 
        # Places cup in Spot 7 (third row, first column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.position.z = .2

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()
        move_group.clear_pose_targets()
        
        spot_seven_down = move_group.get_current_pose().pose

        return spot_seven_down


def spot_9(move_group):
 
        # Places cup in Spot 9 (third row, third column)
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 0
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 1
        pose_goal.orientation.z = 0
        pose_goal.position.x = .85
        pose_goal.position.y = -.15
        pose_goal.position.z = 0.18

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        spot_nine = move_group.get_current_pose().pose
        return spot_nine




def main():

    # Initialize MoveIt
    #input("Initialize MoveIt")
    move_group = __init__()

    gripper = Gripper()
    gripper.gripper_init()

    #uncomment whichever board spot you want to test, just make sure you start with the join_goal command to get to starting point
    #input("Test all spots")
    '''
    joint_goal = go_to_joint_state(move_group)
    pick1 = pick_up1(move_group)
    joint_goal = go_to_joint_state(move_group)
    pick2 = pick_up2(move_group)
    #gripper.OpenGripper()
    #gripper.CloseGripper()
    joint_goal = go_to_joint_state(move_group)
    pick3 = pick_up3(move_group)
    print('gripper open manually')
    time.sleep(5)
    #gripper.OpenGripper()
    #gripper.CloseGripper()
    joint_goal = go_to_joint_state(move_group)
    '''
    '''
    joint_goal = go_to_joint_state(move_group)
    spot_one = spot_1(move_group)
    joint_goal = go_to_joint_state(move_group)
    spot_two = spot_2(move_group)
    joint_goal = go_to_joint_state(move_group)
    spot_three = spot_3(move_group)
    joint_goal = go_to_joint_state(move_group)
    spot_four = spot_4(move_group)
    joint_goal = go_to_joint_state(move_group)
    spot_five = spot_5(move_group)
    joint_goal = go_to_joint_state(move_group)
    spot_six = spot_6(move_group)
    '''
    joint_goal = go_to_joint_state(move_group)
    #current_pose_now = move_group.get_current_pose().pose
    #print(current_pose_now)
    spot_seven = spot_7(move_group)
    spot_seven_down = spot_7down(move_group) #### problem here
    joint_goal = go_to_joint_state(move_group)
    '''
    spot_eight = spot_8(move_group)
    joint_goal = go_to_joint_state(move_group)
    spot_nine = spot_9(move_group)
    joint_goal = go_to_joint_state(move_group)
    '''
    
    

    # Shutdown MoveIt and ROS node
    moveit_commander.roscpp_shutdown()
    rospy.signal_shutdown('Node is shutting down.')

if __name__ == "__main__":
    main()
